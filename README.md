# Advent of Code 2022

Hi, I'm André, a fullstack Elixir/Phoenix developer. I'm doing Advent of Code
for the first time this year!

## Running the code 

To run part 1 for day 3:

``` elixir
mix aoc -d 3 -p 1
```

You can also benchmark the code using Benchee by passing `-b` to the `mix` task:
``` elixir
mix aoc -d 3 -p 1 -b
```

Though this can be really handy to compare implementations, data structures,
tail recursion vs. map/reducing, I do no strive for performance. The only goal
here is to have fun, learn more about my beloved language, and maybe do some
nice refactoring!

