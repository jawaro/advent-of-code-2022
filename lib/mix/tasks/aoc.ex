defmodule Mix.Tasks.Aoc do
  use Mix.Task

  def run(args) do
    parsed_args = parse_args(args)
    day = Keyword.fetch!(parsed_args, :day)
    part = Keyword.fetch!(parsed_args, :part)
    benchmark = Keyword.get(parsed_args, :benchmark, false)

    padded_day_string =
      day
      |> to_string()
      |> String.pad_leading(2, "0")

    atom_part = if part == 1, do: :part1, else: :part2

    module =
      "Elixir.Aoc22.Day"
      |> Kernel.<>(padded_day_string)
      |> String.to_existing_atom()

    part_fun = Function.capture(module, atom_part, 1)

    {:ok, input} = File.read("./lib/aoc22/day#{padded_day_string}.txt")

    if benchmark,
      do: Benchee.run(%{atom_part => fn -> part_fun.(input) end}),
      else:
        input
        |> part_fun.()
        |> IO.inspect(label: "Day #{day} Part #{part} Results")
  end

  defp parse_args(args) do
    args
    |> OptionParser.parse(
      aliases: [d: :day, p: :part, b: :benchmark],
      strict: [day: :integer, part: :integer, benchmark: :boolean]
    )
    |> elem(0)
  end
end
