defmodule Aoc22.Day04 do
  @moduledoc false

  def part1(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&split_string/1)
    |> Enum.map(&string_list_to_intervals/1)
    |> Enum.count(&contained_in_other?/1)
  end

  def part2(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&split_string/1)
    |> Enum.map(&string_list_to_intervals/1)
    |> Enum.count(&overlap?/1)
  end

  defp split_string(string) do
    String.split(string, [",", "-"])
  end

  defp string_list_to_intervals([a, b, c, d]) do
    {{String.to_integer(a), String.to_integer(b)}, {String.to_integer(c), String.to_integer(d)}}
  end

  defp contained_in_other?({{i, j}, {k, l}}) do
    (i >= k and j <= l) or (k >= i and l <= j)
  end

  defp overlap?({{i, j}, {k, l}}) do
    j >= k and l >= i
  end
end
