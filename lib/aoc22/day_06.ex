defmodule Aoc22.Day06 do
  @moduledoc false

  def part1(input) do
    input
    |> String.split("", trim: true)
    |> lookup(4)
  end

  def part2(input) do
    input
    |> String.split("", trim: true)
    |> lookup(14)
  end

  defp lookup(input, marker_length) do
    lookup(input, [], 0, marker_length)
  end

  defp lookup([c | rest], list, i, marker_length) do
    match_index = Enum.find_index(list, &(&1 == c))

    cond do
      match_index ->
        lookup(rest, [c | Enum.take(list, match_index)], i + 1, marker_length)

      length(list) < marker_length - 1 ->
        lookup(rest, [c | list], i + 1, marker_length)

      true ->
        i + 1
    end
  end
end
