defmodule Aoc22.Day11 do
  @moduledoc false

  @worry_drop 3

  def part1(input) do
    input
    |> build_monkeys()
    |> play(20, fn item -> floor(item / @worry_drop) end)
    |> monkey_business_level()
  end

  def part2(input) do
    monkeys = build_monkeys(input)

    lcm =
      monkeys
      |> Map.values()
      |> Enum.map(& &1.test)
      |> Enum.product()

    monkeys
    |> play(10000, fn item -> rem(item, lcm) end)
    |> monkey_business_level()
  end

  defp monkey_business_level(monkeys) do
    monkeys
    |> Enum.map(fn {_, monkey} -> monkey.items_inspected end)
    |> Enum.sort(:desc)
    |> Enum.take(2)
    |> Enum.product()
  end

  defp play(monkeys, rounds, worry_func) when is_integer(rounds) do
    n = map_size(monkeys)

    Enum.reduce(1..rounds, monkeys, fn _, monkeys ->
      Enum.reduce(0..(n - 1), monkeys, fn i, monkeys ->
        monkey_play(monkeys, i, worry_func)
      end)
    end)
  end

  defp monkey_play(monkeys, i, worry_func) do
    monkey = monkeys[i]

    Enum.reduce(monkey.items, monkeys, fn item, monkeys ->
      new_item = worry_func.(monkey.op.(item))
      to = if rem(new_item, monkey.test) == 0, do: monkey.if_true, else: monkey.if_false

      monkey_throw(monkeys, i, to, new_item)
    end)
  end

  defp monkey_throw(monkeys, from, to, item) do
    monkeys
    |> update_in([from, :items], fn [_ | tail] -> tail end)
    |> update_in([from, :items_inspected], &(&1 + 1))
    |> update_in([to, :items], fn items -> items ++ [item] end)
  end

  defp build_monkeys(input) do
    input
    |> String.split("\n\n", trim: true)
    |> Enum.map(&parse_monkey_input/1)
    |> Enum.with_index()
    |> Enum.into(%{}, fn {monkey, index} -> {index, monkey} end)
  end

  defp parse_monkey_input(input) do
    [_monkey, starting_items, operation, test, if_true, if_false] =
      String.split(input, "\n", trim: true)

    starting_items =
      starting_items
      |> String.split(": ")
      |> Enum.at(1)
      |> String.split(", ")
      |> Enum.map(&String.to_integer/1)

    operation =
      operation
      |> String.split("new = old ")
      |> Enum.at(1)
      |> String.split(" ", trim: true)
      |> to_function()

    test =
      test
      |> String.split("divisible by ")
      |> Enum.at(1)
      |> String.to_integer()

    if_true =
      if_true
      |> String.split("monkey ")
      |> Enum.at(1)
      |> String.to_integer()

    if_false =
      if_false
      |> String.split("monkey ")
      |> Enum.at(1)
      |> String.to_integer()

    %{
      items: starting_items,
      op: operation,
      test: test,
      if_true: if_true,
      if_false: if_false,
      items_inspected: 0
    }
  end

  defp to_function(["*", "old"]), do: fn old -> old * old end
  defp to_function(["+", "old"]), do: fn old -> old + old end
  defp to_function(["*", n]), do: fn old -> old * String.to_integer(n) end
  defp to_function(["+", n]), do: fn old -> old + String.to_integer(n) end
end
