defmodule Aoc22.Day07 do
  @moduledoc false

  @ls_regex ~r/(\d+)/
  @total_disk_space 70_000_000
  @needed_space 30_000_000

  def part1(input) do
    input
    |> dir_state()
    |> Enum.filter(fn {_, size} -> size < 100_000 end)
    |> Enum.map(fn {_, size} -> size end)
    |> Enum.sum()
  end

  def part2(input) do
    dir_state = dir_state(input)
    unused_space = @total_disk_space - Map.get(dir_state, [])

    dir_state
    |> Enum.map(fn {_, size} -> size end)
    |> Enum.sort()
    |> Enum.find(&(&1 + unused_space >= @needed_space))
  end

  def dir_state(input) do
    input
    |> String.split("$ ", trim: true)
    |> Enum.map(fn command_and_output ->
      [command | output] =
        command_and_output
        |> String.split("\n", trim: true)

      command = String.split(command, " ")
      {command, output}
    end)
    |> Enum.reduce(%{current_dir: []}, fn {command, output}, state ->
      execute(state, command, output)
    end)
  end

  defp execute(state, ["cd", "/"], _),
    do: %{state | current_dir: []}

  defp execute(%{current_dir: []} = state, ["cd", ".."], _),
    do: state

  defp execute(%{current_dir: [_ | tl]} = state, ["cd", ".."], _),
    do: %{state | current_dir: tl}

  defp execute(%{current_dir: current_dir} = state, ["cd", name], _),
    do: %{state | current_dir: [name | current_dir]}

  defp execute(state, ["ls"], output) do
    ls_size = parse_ls(output)

    state
    |> update_parents_size(ls_size, state.current_dir)
  end

  defp update_parents_size(state, size, []) do
    Map.update(state, [], size, fn current_size -> current_size + size end)
  end

  defp update_parents_size(state, size, [_ | parent] = dir) do
    state
    |> Map.update(dir, size, fn current_size -> current_size + size end)
    |> update_parents_size(size, parent)
  end

  defp parse_ls(ls_output) do
    ls_output
    |> Enum.map(fn line ->
      first =
        @ls_regex
        |> Regex.scan(line, capture: :first)
        |> List.first()

      first && first |> List.first() |> String.to_integer()
    end)
    |> Enum.reject(&(&1 == nil))
    |> Enum.sum()
  end
end
