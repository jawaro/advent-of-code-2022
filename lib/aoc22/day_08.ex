defmodule Aoc22.Day08 do
  @moduledoc false

  @directions [{0, 1}, {0, -1}, {1, 0}, {-1, 0}]

  def part1(input) do
    input
    |> build_matrix()
    |> check_matrix()
    |> Enum.count()
  end

  def part2(input) do
    matrix = build_matrix(input)
    m = tuple_size(matrix)
    n = tuple_size(elem(matrix, 0))

    Enum.reduce(0..(m - 1), 0, fn j, max ->
      Enum.reduce(0..(n - 1), max, fn i, max ->
        score = scenic_score(matrix, {i, j})
        max(max, score)
      end)
    end)
  end

  defp scenic_score(matrix, tree) do
    Enum.reduce(@directions, 1, fn direction, score ->
      score * scenic_score(matrix, tree, direction)
    end)
  end

  def add_coordinates({i, j}, {k, l}), do: {i + k, j + l}

  defp scenic_score(matrix, tree, direction) do
    value = matrix_elem(matrix, tree)
    scenic_score(matrix, value, direction, 0, add_coordinates(tree, direction))
  end

  defp scenic_score(matrix, value, direction, score, tree) do
    cond do
      not valid_tree?(matrix, tree) ->
        score

      matrix_elem(matrix, tree) >= value ->
        score + 1

      true ->
        scenic_score(matrix, value, direction, score + 1, add_coordinates(tree, direction))
    end
  end

  defp valid_tree?(matrix, {i, j}) do
    m = tuple_size(matrix)
    n = tuple_size(elem(matrix, 0))

    0 <= i and i < n and 0 <= j and j < m
  end

  defp matrix_elem(matrix, {i, j}) do
    matrix_elem(matrix, i, j)
  end

  defp matrix_elem(matrix, i, j) do
    matrix
    |> elem(j)
    |> elem(i)
  end

  defp check_matrix(matrix) do
    m = tuple_size(matrix)
    n = tuple_size(elem(matrix, 0))

    %{}
    |> check(matrix, 0..(n - 1), 0..(m - 1))
    |> check(matrix, (n - 1)..0, 0..(m - 1))
    |> check(matrix, 0..(m - 1), 0..(n - 1), true)
    |> check(matrix, (m - 1)..0, 0..(n - 1), true)
  end

  defp check(state, matrix, x_interval, y_interval, transpose \\ false) do
    Enum.reduce(y_interval, state, fn j, state ->
      Enum.reduce(x_interval, {state, -1}, fn i, {state, max} ->
        val = matrix_elem(matrix, i, j)
        {i, j} = if transpose, do: {j, i}, else: {i, j}

        if val > max do
          {Map.put(state, {i, j}, true), val}
        else
          {state, max}
        end
      end)
      |> elem(0)
    end)
  end

  defp build_matrix(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      line
      |> String.split("", trim: true)
      |> Enum.map(&String.to_integer/1)
      |> List.to_tuple()
    end)
    |> List.to_tuple()
  end
end
