defmodule Aoc22.Day02 do
  @moduledoc false

  def part1(input) do
    input
    |> String.split()
    |> Enum.chunk_every(2)
    |> Enum.map(fn [p1, p2] -> score(shape(p1), shape(p2)) end)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> String.split()
    |> Enum.chunk_every(2)
    |> Enum.map(fn [p1, p2] ->
      shape1 = shape(p1)

      shape2 =
        p2
        |> strategy()
        |> shape_for(shape1)

      score(shape1, shape2)
    end)
    |> Enum.sum()
  end

  defp shape("A"), do: :rock
  defp shape("B"), do: :paper
  defp shape("C"), do: :scissors
  defp shape("X"), do: :rock
  defp shape("Y"), do: :paper
  defp shape("Z"), do: :scissors

  defp strategy("X"), do: :lose
  defp strategy("Y"), do: :draw
  defp strategy("Z"), do: :win

  defp shape_for(:draw, shape), do: shape
  defp shape_for(:lose, :scissors), do: :paper
  defp shape_for(:win, :scissors), do: :rock
  defp shape_for(:lose, :paper), do: :rock
  defp shape_for(:win, :paper), do: :scissors
  defp shape_for(:lose, :rock), do: :scissors
  defp shape_for(:win, :rock), do: :paper

  defp play(shape, shape), do: :draw
  defp play(:rock, :scissors), do: :win
  defp play(:scissors, :paper), do: :win
  defp play(:paper, :rock), do: :win
  defp play(_, _), do: :lose

  defp shape_score(:rock), do: 1
  defp shape_score(:paper), do: 2
  defp shape_score(:scissors), do: 3

  defp round_score(:lose), do: 0
  defp round_score(:draw), do: 3
  defp round_score(:win), do: 6

  defp score(p1, p2) do
    shape_score(p2) +
      (p2
       |> play(p1)
       |> round_score())
  end
end
