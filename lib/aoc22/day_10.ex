defmodule Aoc22.Day10 do
  @moduledoc false

  @cycles [20, 60, 100, 140, 180, 220]
  @pixel_width 40

  def part1(input) do
    cycle_values =
      input
      |> build_instructions()
      |> execute()

    Enum.reduce(@cycles, 0, fn cycle, sum -> Enum.at(cycle_values, cycle - 1) * cycle + sum end)
  end

  def part2(input) do
    input
    |> build_instructions()
    |> execute()
    |> Enum.with_index()
    |> Enum.reduce("", fn {sprite_position, cycle}, string ->
      string <>
        if(rem(cycle, @pixel_width) == 0, do: "\n", else: "") <>
        if has_pixel?(sprite_position, cycle), do: "#", else: "."
    end)
    |> IO.puts()
  end

  defp has_pixel?(sprite_position, cycle) do
    pixel_position = pixel_position(cycle)
    abs(pixel_position - sprite_position) <= 1
  end

  defp pixel_position(cycle), do: rem(cycle, @pixel_width)

  defp build_instructions(input) when is_binary(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      line
      |> String.split(" ")
      |> make_command()
    end)
  end

  defp execute(instructions) do
    instructions
    |> Enum.reduce(%{x: 1, cycle: 0, values: []}, fn op, %{x: x, cycle: cycle, values: values} ->
      {new_x, inc} =
        case op do
          :noop -> {x, 1}
          {:addx, v} -> {x + v, 2}
        end

      new_values = List.duplicate(x, inc)

      %{x: new_x, cycle: cycle + inc, values: new_values ++ values}
    end)
    |> Map.get(:values)
    |> Enum.reverse()
  end

  defp make_command(["noop"]), do: :noop
  defp make_command(["addx", v]), do: {:addx, String.to_integer(v)}
end
