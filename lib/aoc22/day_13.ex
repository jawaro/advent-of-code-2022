defmodule Aoc22.Day13 do
  @moduledoc false

  def part1(input) do
    input
    |> parse()
    |> Enum.map(fn [left, right] ->
      compare(left, right)
    end)
    |> Enum.with_index(1)
    |> Enum.filter(fn {order, _} -> order == :right_order end)
    |> Enum.map(fn {_, index} -> index end)
    |> Enum.sum()
  end

  def part2(input) do
    sorted_packets =
      input
      |> parse()
      |> Enum.flat_map(& &1)
      |> List.insert_at(0, [[2]])
      |> List.insert_at(0, [[6]])
      |> Enum.sort(fn left, right -> compare(left, right) == :right_order end)

    first_index = Enum.find_index(sorted_packets, &(&1 == [[2]])) + 1
    second_index = Enum.find_index(sorted_packets, &(&1 == [[6]])) + 1

    first_index * second_index
  end

  defp compare([], []), do: :no_decision
  defp compare([], _), do: :right_order
  defp compare(_, []), do: :bad_order
  defp compare([a | tll], [a | tlr]), do: compare(tll, tlr)

  defp compare([a | tll], [b | tlr]) when is_list(a) and is_list(b) do
    case compare(a, b) do
      :right_order -> :right_order
      :bad_order -> :bad_order
      :no_decision -> compare(tll, tlr)
    end
  end

  defp compare([a | tll], [b | tlr]) when is_integer(a) and is_integer(b) do
    cond do
      a < b -> :right_order
      a > b -> :bad_order
      true -> compare(tll, tlr)
    end
  end

  defp compare([a | tll], b) when is_integer(a) and is_list(b), do: compare([[a] | tll], b)
  defp compare(a, [b | tlr]) when is_list(a) and is_integer(b), do: compare(a, [[b] | tlr])

  defp parse(input) do
    input
    |> String.split("\n\n", trim: true)
    |> Enum.map(fn pair ->
      pair
      |> String.split("\n", trim: true)
      |> Enum.map(fn string ->
        Code.string_to_quoted!(string, existing_atoms_only: true)
      end)
    end)
  end
end
