defmodule Aoc22.Day12 do
  @moduledoc false

  def part1(input) do
    shortest_path(input, ?S, [?E], false)
  end

  def part2(input) do
    shortest_path(input, ?E, [?S, ?a], true)
  end

  defp shortest_path(input, source, targets, reverse) do
    matrix = build_matrix(input)
    graph = build_graph(matrix, reverse: reverse)

    {source, targets} = find_source_and_targets(matrix, source, targets)

    data =
      graph
      |> Graph.dijkstra(source)

    data
    |> Enum.filter(fn {k, v} -> k in targets and v.dist != :infty end)
    |> Enum.map(fn {_, v} -> v.dist end)
    |> Enum.min()
  end

  defp find_source_and_targets(matrix, source, targets)
       when is_tuple(matrix) and is_list(targets) do
    {n, m} = matrix_sizes(matrix)

    chars = [source | targets]

    %{true => source, false => targets} =
      for i <- 0..(n - 1),
          j <- 0..(m - 1),
          matrix_elem(matrix, i, j) in chars do
        {matrix_elem(matrix, i, j), {i, j}}
      end
      |> Enum.group_by(fn {c, _} -> c == source end, fn {_, v} -> v end)

    {hd(source), targets}
  end

  defp build_graph(matrix, options \\ []) when is_tuple(matrix) do
    options = Keyword.merge([reverse: false], options)
    {n, m} = matrix_sizes(matrix)

    for i <- 0..(n - 1), j <- 0..(m - 1) do
      val = matrix_elem(matrix, i, j)

      vertices =
        adjacent_vertices(i, j, n, m)
        |> Enum.filter(fn {k, l} ->
          other = matrix_elem(matrix, k, l)
          elevation = (options[:reverse] && elevation(other, val)) || elevation(val, other)
          elevation <= 1
        end)

      {{i, j}, vertices}
    end
    |> Map.new()
  end

  defp elevation(?S, letter), do: letter - ?a
  defp elevation(letter, ?S), do: ?a - letter
  defp elevation(?E, letter), do: letter - ?z
  defp elevation(letter, ?E), do: ?z - letter
  defp elevation(c1, c2), do: c2 - c1

  defp adjacent_vertices(i, j, n, m) do
    [{i - 1, j}, {i + 1, j}, {i, j + 1}, {i, j - 1}]
    |> Enum.filter(fn {i, j} ->
      0 <= i and i < n and 0 <= j and j < m
    end)
  end

  defp matrix_sizes(matrix) when is_tuple(matrix) do
    # Assuming all tuples have same size.
    m = tuple_size(matrix)
    n = tuple_size(elem(matrix, 0))

    {n, m}
  end

  defp matrix_elem(matrix, i, j) do
    matrix
    |> elem(j)
    |> elem(i)
  end

  defp build_matrix(input) when is_binary(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      line
      |> String.split("", trim: true)
      |> Enum.map(fn string ->
        string
        |> String.to_charlist()
        |> hd()
      end)
      |> List.to_tuple()
    end)
    |> List.to_tuple()
  end
end

defmodule Graph do
  def dijkstra(graph, source) do
    vertices = vertices(graph)

    data =
      vertices
      |> Enum.map(fn vertex ->
        {vertex, %{dist: :infty, prev: nil}}
      end)
      |> Map.new()
      |> put_in([source, :dist], 0)

    queue =
      Enum.reduce(data, [], fn {vertex, values}, queue ->
        add_with_priority(queue, data, vertex, values.dist)
      end)

    do_dijkstra(graph, queue, data)
  end

  defp do_dijkstra(_, [] = _queue, data), do: data

  defp do_dijkstra(graph, queue, data) do
    {queue, u} = extract_min(queue)

    {queue, data} =
      graph
      |> neighbors(u)
      |> Enum.filter(&(&1 in queue))
      |> Enum.reduce({queue, data}, fn v, {queue, data} ->
        new_dist =
          case data[u].dist do
            :infty -> :infty
            n -> n + 1
          end

        # In Elixir int < atom
        if new_dist < data[v].dist do
          queue = decrease_priority(queue, data, v, new_dist)
          data = update_vertex(data, v, new_dist, u)
          {queue, data}
        else
          {queue, data}
        end
      end)

    do_dijkstra(graph, queue, data)
  end

  defp update_vertex(data, v, dist, prev) do
    data
    |> put_in([v, :dist], dist)
    |> put_in([v, :prev], prev)
  end

  defp add_with_priority(queue, data, v, dist) do
    index =
      Enum.find_index(queue, fn vertex ->
        # In Elixir int < atom
        data[vertex].dist >= dist
      end) || 1

    List.insert_at(queue, index, v)
  end

  defp extract_min([u | tail]) do
    {tail, u}
  end

  defp decrease_priority(queue, data, v, priority) do
    queue
    |> List.delete(v)
    |> add_with_priority(data, v, priority)
  end

  defp neighbors(graph, u), do: Map.get(graph, u)

  defp vertices(graph), do: Map.keys(graph)
end
