defmodule Aoc22.Day01 do
  @moduledoc false

  # We could just sort the list and take the first or three first elements, but this
  # is slightly less efficient.

  def part1(input) do
    most_calories(input)
  end

  def part2(input) do
    three_most_calories(input)
  end

  def most_calories(string) when is_binary(string) do
    string
    |> string_to_list()
    |> sum_reduce()
    |> Enum.max()
  end

  def three_most_calories(string) when is_binary(string) do
    string
    |> string_to_list()
    |> sum_reduce()
    |> three_max()
    |> Enum.sum()
  end

  defp string_to_list(string) when is_binary(string) do
    string
    |> String.trim()
    |> String.split("\n")
  end

  defp sum_reduce(list, sep \\ "") when is_list(list) do
    list
    |> Enum.reduce([], fn
      ^sep, [] -> [0]
      ^sep, acc -> [0 | acc]
      calories, [] -> [String.to_integer(calories)]
      calories, [hd | tl] -> [String.to_integer(calories) + hd | tl]
    end)
    |> Enum.reverse()
  end

  defp three_max(list) when is_list(list) and length(list) >= 3 do
    list
    |> Enum.reduce([0, 0, 0], fn calories, [max1, max2, max3] ->
      cond do
        calories > max1 -> [calories, max1, max2]
        calories > max2 -> [max1, calories, max2]
        calories > max3 -> [max1, max2, calories]
        true -> [max1, max2, max3]
      end
    end)
  end
end
