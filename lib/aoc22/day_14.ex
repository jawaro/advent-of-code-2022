defmodule Aoc22.Day14 do
  @moduledoc false

  @sand_source {500, 0}

  def part1(input) do
    input
    |> parse()
    |> run()
  end

  def part2(input) do
    input
    |> parse()
    |> run2()
  end

  def run2(map) do
    floor =
      map
      |> Enum.map(fn {{_, j}, true} -> j end)
      |> Enum.max()
      |> Kernel.+(2)

    run2(map, @sand_source, 0, floor)
  end

  def run2(map, sand, n, floor) do
    case move(map, sand, floor: floor) do
      @sand_source ->
        n + 1

      ^sand ->
        map
        |> add_sand(sand)
        |> run2(@sand_source, n + 1, floor)

      next_sand ->
        run2(map, next_sand, n, floor)
    end
  end

  def run(map), do: run(map, @sand_source, 0)

  def run(map, sand, n) do
    if falling_to_abyss?(map, sand) do
      n
    else
      case move(map, sand) do
        ^sand ->
          map
          |> add_sand(sand)
          |> run(@sand_source, n + 1)

        next_sand ->
          run(map, next_sand, n)
      end
    end
  end

  def add_sand(map, sand), do: Map.put(map, sand, true)

  def parse(input) when is_binary(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.flat_map(fn line ->
      line
      |> String.split([",", " -> "], trim: true)
      |> Enum.chunk_every(2)
      |> Enum.map(fn [i, j] ->
        {String.to_integer(i), String.to_integer(j)}
      end)
      |> Enum.chunk_every(2, 1, :discard)
      |> Enum.flat_map(fn [p1, p2] -> points(p1, p2) end)
    end)
    |> Map.new(fn point -> {point, true} end)
  end

  defp move(map, point, opts \\ []) do
    cond do
      empty?(map, down(point), opts) -> down(point)
      empty?(map, down_left(point), opts) -> down_left(point)
      empty?(map, down_right(point), opts) -> down_right(point)
      true -> point
    end
  end

  defp falling_to_abyss?(map, {i, j}) do
    Enum.all?(map, fn
      {{^i, l}, true} -> l < j
      {_, true} -> true
    end)
  end

  defp down({i, j}), do: {i, j + 1}
  defp down_left({i, j}), do: {i - 1, j + 1}
  defp down_right({i, j}), do: {i + 1, j + 1}

  defp empty?(_, {_, floor} = _point, floor: floor), do: false
  defp empty?(map, point, _opts), do: Map.get(map, point, false) == false

  defp points({i, j1}, {i, j2}) do
    for j <- j1..j2 do
      {i, j}
    end
  end

  defp points({i1, j}, {i2, j}) do
    for i <- i1..i2 do
      {i, j}
    end
  end
end
