defmodule Aoc22.Day03 do
  @moduledoc false

  def part1(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&split_string/1)
    |> Enum.map(fn {c1, c2} -> priority_of_intersection([c1, c2]) end)
    |> Enum.sum()
  end

  def part2(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.chunk_every(3)
    |> Enum.map(&priority_of_intersection/1)
    |> Enum.sum()
  end

  defp priority_of_intersection(list) do
    list
    |> Enum.map(&string_to_set/1)
    |> common_item()
    |> priority()
  end

  defp split_string(string) when is_binary(string) do
    half = div(String.length(string), 2)
    String.split_at(string, half)
  end

  defp string_to_set(string) when is_binary(string) do
    string
    |> String.split("", trim: true)
    |> MapSet.new()
  end

  defp common_item([first, second | rest]) do
    rest
    |> Enum.reduce(MapSet.intersection(first, second), &MapSet.intersection/2)
    # We could use Map.fetch(O) instead, but this would break the nice pipeline.
    # Since the MapSet will be constant, converting to a list to retrieve the
    # first element costs nothing.
    |> MapSet.to_list()
    |> List.first()
    |> String.to_charlist()
    |> List.first()
  end

  def priority(type) when type >= ?a and type <= ?z, do: type - ?a + 1
  def priority(type) when type >= ?A and type <= ?Z, do: type - ?A + 27
end
