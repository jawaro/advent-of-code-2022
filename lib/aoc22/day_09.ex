defmodule Aoc22.Day09.Coordinate do
  defstruct i: 0, j: 0

  @type t :: %__MODULE__{
          i: non_neg_integer(),
          j: non_neg_integer()
        }

  def new(i \\ 0, j \\ 0) when is_integer(i) and is_integer(j) do
    %__MODULE__{i: i, j: j}
  end

  def add(%__MODULE__{i: i, j: j}, %__MODULE__{i: k, j: l}) do
    %__MODULE__{i: i + k, j: j + l}
  end

  def i(%__MODULE__{i: i}), do: i
  def j(%__MODULE__{j: j}), do: j

  # def decompose(%__MODULE__{i: 0, j: 0}), do: []
  # def decompose(%__MODULE__{i: _, j: 0} = coordinate), do: [coordinate]
  # def decompose(%__MODULE__{i: 0, j: _} = coordinate), do: [coordinate]

  # def decompose(%__MODULE__{i: i, j: j}) do
  #   [
  #     %__MODULE__{i: 0, j: j},
  #     %__MODULE__{i: i, j: 0}
  #   ]
  # end

  def to_tuple(%__MODULE__{i: i, j: j}), do: {i, j}

  def to_string(%__MODULE__{i: 1, j: 0}), do: "R"
  def to_string(%__MODULE__{i: -1, j: 0}), do: "L"
  def to_string(%__MODULE__{i: 0, j: 1}), do: "U"
  def to_string(%__MODULE__{i: 0, j: -1}), do: "D"
end

defmodule Aoc22.Day09.State do
  alias Aoc22.Day09.Coordinate

  defstruct head: Coordinate.new(),
            tail: Coordinate.new(),
            delta: Coordinate.new(),
            last_move: Coordinate.new(),
            visited: %{{0, 0} => true}

  def new(), do: %__MODULE__{}

  def next(%__MODULE__{} = state, %Coordinate{} = move) do
    next_delta = Coordinate.add(state.delta, move)
    new_head = Coordinate.add(state.head, move)

    if too_far?(next_delta) do
      tail_move =
        if diagonal?(move) do
          move
        else
          state.delta
        end

      new_delta =
        if diagonal?(move) do
          state.delta
        else
          move
        end

      new_tail = Coordinate.add(state.tail, tail_move)

      %__MODULE__{
        state
        | head: new_head,
          tail: new_tail,
          delta: new_delta,
          last_move: tail_move
      }
      |> add_visisted(new_tail)
    else
      %__MODULE__{
        state
        | head: new_head,
          delta: Coordinate.add(state.delta, move),
          last_move: Coordinate.new()
      }
    end
  end

  def number_of_visisted_positions(%__MODULE__{visited: visited}), do: Enum.count(visited)

  def last_move(%__MODULE__{last_move: last_move}), do: last_move

  defp too_far?(%Coordinate{i: i, j: j}) when abs(i) > 1 or abs(j) > 1, do: true
  defp too_far?(%Coordinate{}), do: false
  defp diagonal?(%Coordinate{i: i, j: j}) when abs(i) == 1 and abs(j) == 1, do: true
  defp diagonal?(_), do: false

  defp add_visisted(%__MODULE__{visited: visited} = state, %Coordinate{} = coordinate) do
    %{state | visited: Map.put(visited, Coordinate.to_tuple(coordinate), true)}
  end
end

defmodule Aoc22.Day09 do
  @moduledoc false

  alias Aoc22.Day09.Coordinate
  alias Aoc22.Day09.State

  @up Coordinate.new(0, 1)
  @down Coordinate.new(0, -1)
  @right Coordinate.new(1, 0)
  @left Coordinate.new(-1, 0)

  def part1(input) do
    input
    |> build_direction_list()
    |> simulate()
    |> elem(0)
    |> State.number_of_visisted_positions()
  end

  def part2(input) do
    input
    |> build_direction_list()
    |> simulate(9)
  end

  @spec simulate(list(Coordinate.t()), non_neg_integer()) :: non_neg_integer()
  defp simulate(direction_list, n) when is_list(direction_list) and is_integer(n) and n >= 1 do
    Enum.reduce(1..n, {nil, direction_list}, fn i, {_, directions} ->
      res = simulate(directions)

      res
      |> elem(0)
      |> draw_visited()

      # |> Enum.reject(&(&1 == Coordinate.new()))
      # |> Enum.reduce(Coordinate.new(), &Coordinate.add/2)
      # |> IO.inspect(label: i)

      res
    end)
    |> elem(0)
    |> draw_visited()

    # |> State.number_of_visisted_positions()

    # direction_list
    # |> Enum.reduce(initial_states, fn move, states ->
    #   IO.puts("")
    #   IO.inspect(Coordinate.to_tuple(move), label: "MOVE")

    #   states
    #   |> Enum.reduce({move, []}, fn state, {move, new_states} ->
    #     new_state = State.next(state, move)
    #     next_move = State.last_move(new_state)
    #     IO.inspect(Coordinate.to_tuple(next_move), label: "  NEXT")
    #     {next_move, [new_state | new_states]}
    #   end)
    #   |> elem(1)
    #   |> Enum.reverse()
    # end)
    # |> List.last()
    # |> State.number_of_visisted_positions()
  end

  defp draw_visited(state) do
    state.visited

    for j <- 15..-7 do
      for i <- -20..20 do
        if Map.get(state.visited, {i, j}, false) do
          "#"
        else
          "."
        end
      end
      |> to_string()
      |> Kernel.<>("\n")
    end
    |> to_string()
    |> IO.puts()
  end

  @spec simulate(list(Coordinate.t())) :: State.t()
  defp simulate(direction_list) when is_list(direction_list) do
    {state, move_list} =
      direction_list
      |> Enum.reduce({State.new(), []}, fn move, {state, move_list} ->
        new_state = State.next(state, move)
        last_move = State.last_move(new_state)

        {new_state, [last_move | move_list]}
      end)

    {state, Enum.reverse(move_list)}
  end

  defp build_direction_list(input) when is_binary(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.flat_map(fn line ->
      [direction, count] = String.split(line, " ", trim: true)
      direction_list(direction, String.to_integer(count))
    end)
  end

  defp direction_list("R", count), do: for(_ <- 1..count, do: @right)
  defp direction_list("L", count), do: for(_ <- 1..count, do: @left)
  defp direction_list("U", count), do: for(_ <- 1..count, do: @up)
  defp direction_list("D", count), do: for(_ <- 1..count, do: @down)
end
