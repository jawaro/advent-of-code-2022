defmodule Aoc22.Day05 do
  @moduledoc false

  def part1(input) do
    input
    |> state_and_instructions()
    |> execute_instructions(&Enum.reverse/1)
  end

  def part2(input) do
    input
    |> state_and_instructions()
    |> execute_instructions(fn x -> x end)
  end

  defp initial_state(state_string, stacks_number) do
    state_string
    |> Enum.map(fn stack ->
      ~r/(    |[[:upper:]])/
      |> Regex.scan(stack, capture: :first)
      |> Enum.map(fn captured ->
        captured
        |> List.first()
        |> String.trim()
      end)
    end)
    |> Enum.map(fn stack -> list_pad(stack, stacks_number) end)
    |> Enum.map(&List.to_tuple/1)
    |> Enum.reverse()
    |> make_state(stacks_number)
  end

  defp make_state(stack_by_line, stacks_number) do
    make_state(stack_by_line, 0, stacks_number, %{})
  end

  defp make_state(_, stacks_number, stacks_number, state), do: state

  defp make_state(stack_by_line, i, stacks_number, state) do
    stack =
      Enum.reduce(stack_by_line, Stack.new(), fn slice, stack ->
        case elem(slice, i) do
          "" -> stack
          letter -> Stack.push(stack, [letter])
        end
      end)

    state = Map.put(state, i, stack)
    make_state(stack_by_line, i + 1, stacks_number, state)
  end

  defp state_and_instructions(input) do
    [state, instructions] = String.split(input, "\n\n")

    {stacks_number, state} =
      state
      |> String.split("\n")
      |> List.pop_at(-1)

    stacks_number =
      stacks_number
      |> String.split(" ", trim: true)
      |> length()

    state = initial_state(state, stacks_number)
    instructions = build_instructions(instructions)

    {state, instructions}
  end

  defp build_instructions(instructions_input) when is_binary(instructions_input) do
    instructions_input
    |> String.split("\n", trim: true)
    |> Enum.map(fn instruction ->
      ~r/(\d+)/
      |> Regex.scan(instruction)
      |> Enum.map(fn captured ->
        captured
        |> List.first()
      end)
      |> Enum.map(fn string -> String.to_integer(string) end)
    end)
  end

  def execute_instructions({state, instructions}, move_function) do
    Enum.reduce(instructions, state, fn [n, from, to], state ->
      move(state, n, from - 1, to - 1, move_function)
    end)
    |> Enum.flat_map(fn {_, stack} ->
      {first, _} = Stack.pop(stack)
      first
    end)
    |> List.to_string()
  end

  defp list_pad(list, n) do
    l = length(list)

    if l < n do
      list ++ for _ <- (length(list) + 1)..n, do: ""
    else
      list
    end
  end

  defp move(state, n, from, to, move_function) do
    {crates, new_from} = Stack.pop(Map.get(state, from), n)
    new_to = Stack.push(Map.get(state, to), move_function.(crates))

    state
    |> Map.put(from, new_from)
    |> Map.put(to, new_to)
  end
end

defmodule Stack do
  defstruct elements: []

  def new do
    %__MODULE__{elements: []}
  end

  def pop(%__MODULE__{elements: elements} = stack, n \\ 1) do
    {first, second} = Enum.split(elements, n)
    {first, %{stack | elements: second}}
  end

  def push(%__MODULE__{elements: elements} = stack, list) do
    %{stack | elements: list ++ elements}
  end
end
